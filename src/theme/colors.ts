import { TThemeColors } from "../utils/convertThemeColorsToRootColors";

export const colors: TThemeColors = {
  ui: {
    // primary: "#5865FF",
    red: {
      normal: "rgba(193, 41, 46, 1)",
      lighter: "rgba(193, 41, 46, 0.1)",
    },
    blue: {
      normal: "rgba(93, 183, 222, 1)",
      lighter: "rgba(93, 183, 222, 0.1)",
    },
    yellow: {
      normal: "rgba(241, 211, 2, 1)",
      lighter: "rgba(241, 211, 2, 0.1)",
    },
    green: {
      normal: "rgba(12, 245, 116, 1)",
      lighter: "rgba(12, 245, 116, 0.1)",
    },
    white: {
      normal: "white",
      lighter: "rgba(255, 255, 255, 0.1)",
    },
  },
};

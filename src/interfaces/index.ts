export interface IAPIResponseInterface {
  data: any | null;
  error: boolean;
  message: string;
}
